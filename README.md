## :fire: Is It Firing? :fire:

The concept for this project is a web-app for global surfing enthusiasts to use. Users can access the site and click on images of 6 of the top big-wave surfing destinations of the world. Using the Open Weather API, the page pulls in weather data, and based on the favorability of the conditions displays a random image (from the Pexels API,) of 'happy,' :smiley: if the conditions are favorable, or 'sad,' :cry: if the conditions are not. 

This project is the product of a Hackathon in the Hack Reactor full-stack web development bootcamp in the summer of 2023. 

The criteria was to build a React project using 2 different API's to pull in data, and we had 2.5 hrs to conceptualize, build, test, and present it. Our team consisted of 2 people: myself, and @spidergrrljess.

Big Wave Spots Include:
* Nazare, Portugal
* Cloudbreak, Fiji
* Mavericks, California
* Pipeline, Hawaii
* Puerto Escondido, Mexico
* Tehapo'o, Tahiti


<br>

## Team Member Roles
Jessica:
* laying down a React baseline code structure
* throwing together a quick UI to present content in a clear way
* initial logic and component structure
* api testing

Ian:
* project concept
* api research and testing
* React component refactoring
* QC and testing

<br>

## API References

* www.https://www.pexels.com/api/documentation/
* https://openweathermap.org/api/one-call-3

<br>

## Demo

Home Page:

![Screenshot_2023-10-21_at_11.32.55_AM](/uploads/9554ed88916a4ba76d96677dfd7893e0/Screenshot_2023-10-21_at_11.32.55_AM.png)

<br>

The display for unfavorable conditions:

![Screenshot_2023-10-21_at_11.33.12_AM](/uploads/0396f17d753688aba408edf7d64ab773/Screenshot_2023-10-21_at_11.33.12_AM.png)

<br>

The display for favorable conditions:
![Screenshot_2023-10-21_at_11.33.34_AM](/uploads/1e17b845bf7bb65c603bebcb7102ae11/Screenshot_2023-10-21_at_11.33.34_AM.png)

